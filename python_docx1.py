# -*- coding: utf-8 -*-
"""
Created on Sun Dec 10 01:32:09 2023

@author: User
"""
import docx
from docx.shared import Cm
from docx.oxml.ns import qn
from docx.enum.section import WD_ORIENTATION, WD_SECTION


doc = docx.Document()

doc.add_section(start_type=WD_SECTION.NEW_PAGE)
doc.add_section(start_type=WD_SECTION.NEW_PAGE)
doc.add_section(start_type=WD_SECTION.NEW_PAGE)
doc.add_section(start_type=WD_SECTION.NEW_PAGE)
doc.add_section(start_type=WD_SECTION.NEW_PAGE)


doc.sections[0].page_height = Cm(29.7)  # 設定A4紙的高度
doc.sections[0].page_width = Cm(21)  # 設定A4紙的寬
doc.sections[0].orientation = WD_ORIENTATION.PORTRAIT # 設定紙張方向為橫向，可以不設定 預設為橫向
doc.sections[1].page_height = Cm(21)  # 設定A4紙的高度
doc.sections[1].page_width = Cm(29.7)  # 設定A4紙的寬
doc.sections[1].orientation = WD_ORIENTATION.LANDSCAPE # 設定紙張方向為縱向　

doc.sections[1]._sectPr.xpath('./w:cols')[0].set(qn('w:num'), '2') #把第二節設定為2欄
doc.sections[2]._sectPr.xpath('./w:cols')[0].set(qn('w:num'), '3') #把第二節設定為2欄
doc.sections[3]._sectPr.xpath('./w:cols')[0].set(qn('w:num'), '1') #把第二節設定為2欄
doc.sections[3]._sectPr.xpath('./w:cols')[0].set(qn('w:num'), '3') #把第二節設定為2欄


doc.save('demo1.docx')