# -*- coding: utf-8 -*-
"""
Created on Wed Dec  6 14:04:31 2023

@author: User
"""

from pptx import Presentation
from pptx.util import Pt,Cm
import gorilla
import content_placeholder_patch
patches = gorilla.find_patches([content_placeholder_patch])
for patch in patches:
    gorilla.apply(patch)


ppt = Presentation('ITRI_template.pptx')
title_slide_layout = ppt.slide_layouts[0]
slide = ppt.slides.add_slide(title_slide_layout)
title = slide.shapes.title
subtitle = slide.placeholders[1]

title.text = "Hello, World!"
subtitle.text = "python-pptx was here!"

# 设置添加到当前ppt哪一页
singleLineContent = "我是单行内容"
multiLineContent = \
"""我是多行内容1
我是多行内容2
我是多行内容3
"""

# 获取需要添加文字的页面对象
slide = ppt.slides.add_slide(ppt.slide_layouts[1])
slide = ppt.slides[1]

# 添加单行内容

# 设置添加文字框的位置以及大小
left, top, width, height = Cm(16.9), Cm(1), Cm(12), Cm(1.2)
# 添加文字段落
new_paragraph1 = slide.shapes.add_textbox(left=left, top=top, width=width, height=height).text_frame
# 设置段落内容
new_paragraph1.paragraphs[0].text = singleLineContent
# 设置文字大小
new_paragraph1.paragraphs[0].font.size = Pt(15)


# 添加多行

# 设置添加文字框的位置以及大小
left, top, width, height = Cm(16.9), Cm(3), Cm(12), Cm(3.6)
# 添加文字段落
new_paragraph2 = slide.shapes.add_textbox(left=left, top=top, width=width, height=height).text_frame
# 设置段落内容
new_paragraph2.paragraphs[0].text = multiLineContent
# 设置文字大小
new_paragraph2.paragraphs[0].font.size = Pt(15)


slide = ppt.slides.add_slide(ppt.slide_layouts[1])
slide = ppt.slides[2]

img_name  = 'pic/Space.png'
# 设置位置
left, top, width, height = Cm(6), Cm(6), Cm(20), Cm(9)
# 进行添加
slide.shapes.add_picture(image_file=img_name,left=left,top=top,width=width,height=height)

slide = ppt.slides.add_slide(ppt.slide_layouts[2])
slide = ppt.slides[3]
for shape in slide.placeholders:
    print('%d %s %s' % (shape.placeholder_format.idx, shape.placeholder_format.type, shape.name))
image = slide.placeholders[2].insert_picture('pic/Space.png')
# slide.placeholders[1].text = f"aaa"

slide = ppt.slides.add_slide(ppt.slide_layouts[3])

ppt.save('demo.pptx')
