from docx import Document
from docx.shared import Inches
from docx import Document
from docx.oxml import OxmlElement
from docx.shared import Pt
from docx.oxml.ns import qn
import docx

doc=""


def add_separate():
    doc.add_paragraph('')
    doc.add_paragraph('')


def add_heading(desc, lvl):
    doc.add_heading(desc, level=lvl)

def add_paragraph(desc):
    p = doc.add_paragraph(desc)
    return p
def add_list_bullet(desc):
    p = doc.add_paragraph(desc, style='List Bullet')
    return p
def add_list_number(desc):
    p = doc.add_paragraph(desc, style='List Number')
    return p

def add_caption(target, desc, level):
    """
    Based on: https://github.com/python-openxml/python-docx/issues/359
    """

    # caption type
    paragraph = doc.add_paragraph(f'{target} ', style='Caption')

    # numbering field
    run = paragraph.add_run()
    
    if level==1:
        for n1 in range(1, level+1, 1):
            fldChar = docx.oxml.OxmlElement('w:fldChar')
            fldChar.set(docx.oxml.ns.qn('w:fldCharType'), 'begin')
            run._r.append(fldChar)
        
            instrText = docx.oxml.OxmlElement('w:instrText')
            instrText.text = f' STYLEREF {n1} \s '
            run._r.append(instrText)
    
    
            fldChar = docx.oxml.OxmlElement('w:fldChar')
            fldChar.set(docx.oxml.ns.qn('w:fldCharType'), 'end')
            run._r.append(fldChar)
    
            # add dash between chapter and seq
            paragraph.add_run(f'.')
            # numbering field
            run = paragraph.add_run()

    fldChar = docx.oxml.OxmlElement('w:fldChar')
    fldChar.set(docx.oxml.ns.qn('w:fldCharType'), 'begin')
    # paragraph.add_run(f'.')
    run._r.append(fldChar)

    instrText = docx.oxml.OxmlElement('w:instrText')
    instrText.text = f'SEQ {target} \\* ARABIC \s 1'
    run._r.append(instrText)


    fldChar = docx.oxml.OxmlElement('w:fldChar')
    fldChar.set(docx.oxml.ns.qn('w:fldCharType'), 'end')
    run._r.append(fldChar)




    # caption text
    paragraph.add_run(f' {desc}')


def add_pic(file):
    p = doc.add_picture(file, width=Inches(1.25))
    return p

def add_table(rows, cols):
    tbl = doc.add_table(rows=rows, cols=cols)
    return tbl

def set_cell_border(cell, **kwargs):
    """
    Set cell's border
    Usage:

    set_cell_border(
        cell,
        top={"sz": 12, "val": "single", "color": "#FF0000"},
        bottom={"sz": 12, "color": "#00FF00", "space": "0"}
    )
    """
    tc = cell._tc
    tcPr = tc.get_or_add_tcPr()

    # Border codes
    borders = {
        "top": "top",
        "end": "right",
        "bottom": "bottom",
        "start": "left"
    }

    for key, value in kwargs.items():
        border_name = borders[key]
        border_elm = OxmlElement('w:' + border_name)

        # Check for each attribute in the border element
        for k, v in value.items():
            if k == "sz":
                sz = OxmlElement('w:sz')
                sz.set(qn('w:val'), str(v))
                border_elm.append(sz)
            elif k == "val":
                val = OxmlElement('w:val')
                val.set(qn('w:val'), v)
                border_elm.append(val)
            elif k == "color":
                color = OxmlElement('w:color')
                color.set(qn('w:val'), v)
                border_elm.append(color)
            elif k == "space":
                space = OxmlElement('w:space')
                space.set(qn('w:val'), v)
                border_elm.append(space)

        tcPr.append(border_elm)

def set_table_border(table):
    for row in table.rows:
        for cell in row.cells:
            set_cell_border(
                cell,
                top={"sz": 24, "val": "single", "color": "#FF0000"},  # 12pt, single black border
                bottom={"sz": 24, "val": "single", "color": "#000000"},
                start={"sz": 24, "val": "single", "color": "#000000"},
                end={"sz": 24, "val": "single", "color": "#000000"}
            )

