# -*- coding: utf-8 -*-
"""
Created on Sat Dec  9 22:06:33 2023

@author: User
"""
from docx import Document
from docx.oxml import OxmlElement
from docx.oxml.ns import qn
from docx.text.paragraph import Paragraph
from docx.shared import Cm
from docx.enum.section import WD_ORIENTATION, WD_SECTION
import sys

document = Document()

document.add_section(start_type=WD_SECTION.NEW_PAGE)

document.sections[1]._sectPr.xpath('./w:cols')[0].set(qn('w:num'), '2') 
document.sections[1]._sectPr.xpath('./w:cols')[0].set(qn('w:space'), '1') 

# Add some text to the first column
for i in range(100):    
    paragraph = document.add_paragraph(
        "This is some text.This is some text.This is some text.This is some text")
    paragraph.paragraph_format.space_after = 0


document.add_section(start_type=WD_SECTION.NEW_PAGE)
document.sections[2]._sectPr.xpath('./w:cols')[0].set(qn('w:num'), '1') 
for i in range(100):
    
    paragraph = document.add_paragraph(
        "This is some text.This is some text.This is some text.This is some text")
    paragraph.paragraph_format.space_after = 0

document.save('demo.docx')